<?php 

//[SECTION] Objects as Variables


$buildingObj = (object)[

	'name' => 'Caswynn Building',
	'floors' => 8,
	'address' => (object)[
		'street' => 'Timog Ave.',
		'city' => 'Quezon City',
		'country' => 'Philippines'
	]

];

//[SECTION] Objects from Classes

class Building {
	//properties
	public $name;
	public $floors;
	public $address;

	//methods
	// function setName($name){
	// 	$this->name =$name;
	// }

	//constructor - used to initialize property values open creation of an object.
	public function __construct($name,$floors,$address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	function getDetails(){
		return "The name of the building is $this->name. It has $this->floors floors. It is located at $this->address" ;
	}
}

//[SECTION] Inheritance and Polymorphism

class Condominium extends Building {
	//$name,$floors and $address are inherited from Building to this new child class
	//Meaning all condominium objects that we create will also have these properties

	//The getDetails method below overried the behavior of the parent class' getDetails method via polymorphism
	public function getDetails(){
		return "The name of the Condominium is $this->name. It features $this->floors floors. It's address is $this->address" ;
	}
}



//create a new Object using the Building class
$bldgOne = new Building("Enzo Building",5,"Makati City, Philippines");
//call bldgOne's setName method to give it a name
// $bldgOne->setName("Caswynn Building");

$condoOne = new Condominium("Enzo Condo",5,"Makati City, Philippines");
