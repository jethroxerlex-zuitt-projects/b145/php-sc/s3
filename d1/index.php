<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S3: Classes and Objects</title>
</head>
<body>
	<h1>Objects from a Variable</h1>
	<p><?= $buildingObj->name; ?></p>

	<h1>Objects from Classes</h1>
	<p><?= $bldgOne->getDetails(); ?></p>

	<h1>Inheritance</h1>
	<p><?= $condoOne->name; ?></p>
	<p><?= $condoOne->floors; ?></p>

	<h1>Polymorphism</h1>
	<p><?= $condoOne->getDetails(); ?></p>
</body>
</html>