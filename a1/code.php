<?php

class Person {
	//props
	public $firstName;
	public $middleName;
	public $lastName;

	//constructor
	public function __construct($firstName,$middleName,$lastName){
		$this->firstName =$firstName;
		$this->middleName =$middleName;
		$this->lastName =$lastName;
	}
	function printName(){
		return "Your full name is $this->firstName $this->middleName $this->lastName.";
	}
}

class Developer extends Person {
	public function printName(){
		return "Your name is $this->firstName $this->middleName $this->lastName. and you are a developer";
 }
}

class Engineer extends Person {
	public function printName(){
		return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
 }
}


$personOne = new Person("Tony", "M" ,"Stark");
$developerOne = new Developer("Iron", "M","an");
$engineerOne = new Engineer("Bob", "B","uilder");