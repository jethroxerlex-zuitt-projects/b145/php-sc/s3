<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S3:Classes and Objects Activity</title>
</head>
<body>
	<h2>Person</h2>
	<p><?= $personOne->printName(); ?></p>

	<h2>Developer</h2>
	<p><?= $developerOne->printName(); ?></p>

	<h2>Engineer</h2>
	<p><?= $engineerOne->printName(); ?></p>
	
</body>
</html>